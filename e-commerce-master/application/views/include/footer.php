<?php
/**
 * Created by PhpStorm.
 * User: asamad
 * Date: 6/22/17
 * Time: 3:13 AM
 */
?>

<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-about-us">
                    <h2>Metode <span>Mumet Club</span></h2>
                    <p>Dodolan hp lur , mantul</p>
                    <div class="footer-social">
                        <strong>Our Social Media Networks</strong>
                        <a href="https://www.google.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                        <!--<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>-->
                    </div>
                </div>
            </div>



<!-- 
            <div  class="col-md-3 pull-right col-sm-6">
                <div  class="footer-newsletter">
                    <h2  class="footer-wid-title">Newsletter</h2>
                    <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                    <div class="newsletter-form">
                        <form action="#">
                            <input type="email" placeholder="Type your email">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy;  2018 Mumet.com by <a href="http://" target="_blank">yoggu</a></p>
                </div>
            </div>

        </div>
    </div>
</div> <!-- End footer bottom area -->
