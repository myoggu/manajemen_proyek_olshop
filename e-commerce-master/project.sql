-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2018 at 04:33 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `c_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `u_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`c_id`, `p_id`, `product_name`, `product_price`, `file_name`, `quantity`, `u_id`) VALUES
(2, 1, '11', '1', 'Untitled_Diagram_(2).png', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pending_orders`
--

CREATE TABLE `pending_orders` (
  `po_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `cart_total` varchar(100) NOT NULL,
  `bill_fname` varchar(100) NOT NULL,
  `bill_lname` varchar(100) NOT NULL,
  `bill_add` varchar(100) NOT NULL,
  `bill_city` varchar(100) NOT NULL,
  `bill_email` varchar(100) NOT NULL,
  `bill_phone` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pending_orders`
--

INSERT INTO `pending_orders` (`po_id`, `u_id`, `cart_total`, `bill_fname`, `bill_lname`, `bill_add`, `bill_city`, `bill_email`, `bill_phone`, `status`, `date`) VALUES
(1, 2, '1009', 'sad', 'asd', 'asd', 'que', 'as@as.com', '134566', 'delivered', '2018-12-20 01:44:23'),
(2, 2, '1001', 'a', 'a', 'a', 'que', 'as@as.com', '2', 'delivered', '2018-12-20 09:43:40'),
(3, 2, '1003', 'sad', 'asd', '123', 'que', 'yoggy@gmail.com', '123', '', '2018-12-20 07:03:04'),
(4, 2, '9001000', 'jn', 'jkn', 'jk', 'que', 'as@as.com', '6456', 'delivered', '2018-12-20 09:50:57'),
(5, 3, '9001000', 'asd', 'asd', 'asdasd', 'que', 'myogguu@gmail.com', '45', 'delivered', '2018-12-20 09:44:01'),
(6, 2, '401000', '1', '1', '1', 'isl', 'as@as.com', '1', '', '2018-12-20 15:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_category` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `product_des` varchar(1000) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `file_ext` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `product_name`, `product_category`, `product_price`, `product_des`, `file_name`, `file_path`, `file_ext`) VALUES
(1, '11', 'mobile', '1', '1', 'Untitled_Diagram_(2).png', 'C:/xampp/htdocs/proyek_olshop_new_ci/e-commerce-master/uploads/', '.png'),
(2, 'Samsung', 'mobile', '400000', 'asdasdsad', 'slide-1.jpg', 'C:/xampp/htdocs/proyek_olshop_garap_sampai_mumet/e-commerce-master/uploads/', '.jpg'),
(3, 'samsung', 'mobile', '400000', 'lajsbndanjfnalnldanlfda', 'sam.png', 'C:/xampp/htdocs/proyek_olshop_garap_sampai_mumet/e-commerce-master/uploads/', '.png'),
(4, 'Samsung s9', 'tablet', '9000000', 'mantap', 'Samsung_Galaxy_S9_L_1_(1).jpg', 'C:/xampp/htdocs/proyek_olshop_garap_sampai_mumet/e-commerce-master/uploads/', '.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_history`
--

CREATE TABLE `purchase_history` (
  `ph_id` int(11) NOT NULL,
  `oh` int(11) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_price` varchar(100) NOT NULL,
  `p_qty` varchar(100) NOT NULL,
  `u_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_history`
--

INSERT INTO `purchase_history` (`ph_id`, `oh`, `p_name`, `p_price`, `p_qty`, `u_id`, `date`) VALUES
(1, 1, '11', '1', '9', 2, '2018-12-20 01:41:25'),
(2, 2, '11', '1', '1', 2, '2018-12-20 05:39:25'),
(3, 3, '11', '1', '3', 2, '2018-12-20 07:03:04'),
(4, 4, 'Samsung s9', '9000000', '1', 2, '2018-12-20 09:27:41'),
(5, 5, 'Samsung s9', '9000000', '1', 3, '2018-12-20 09:42:18'),
(6, 6, 'Samsung', '400000', '1', 2, '2018-12-20 15:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `fname`, `lname`, `username`, `email`, `password`, `status`) VALUES
(1, 'Moch', 'Yoggy', 'mumet', 'as@as.com', 'admin', 'admin'),
(2, 'Siti', 'wik wik', 'wik wik', 'a@a.com', 'user', ''),
(3, 'aku', 'siapa', 'aku', 'abc@gmail.com', 'user', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `pending_orders`
--
ALTER TABLE `pending_orders`
  ADD PRIMARY KEY (`po_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `purchase_history`
--
ALTER TABLE `purchase_history`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_history`
--
ALTER TABLE `purchase_history`
  MODIFY `ph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
